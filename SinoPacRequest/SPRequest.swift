//
//  SPRequest.swift
//  SinoPac
//
//  Created by Wen Chi on 2024/3/2.
//

import Foundation
import Alamofire

class Network {
    
        typealias SuccessHandlerType = ((New) -> Void)
        typealias FailureHandlerType = ((Any?, String) ->Void)
        
        private var requestType: HTTPMethod = .post
        private var url: String?
        private var params: [String: Any]?
        private var success: SuccessHandlerType?
        private var failure: FailureHandlerType?
        private var httpRequest: Request?
        private var headers: HTTPHeaders?
        private var jsonDecoder: JSONDecoder?
    
    }

extension Network {

    func url(_ url: String?) -> Self {
        self.url = url ?? ""
        return self
    }
    
    func requestType(_ type: HTTPMethod) -> Self {
        self.requestType = type
        return self
    }
    
    func params(_ params: [String: Any]?) -> Self {
        self.params = params
        return self
    }

    func headers(_ headers: HTTPHeaders?) -> Self {
        self.headers = headers
        return self
    }
    
    func success(_ handler: @escaping SuccessHandlerType) -> Self {
        self.success = handler
        return self
    }
    
    func failure(handler: @escaping FailureHandlerType) -> Self {
        self.failure = handler
        return self
    }
    
    func decoder(_ jsonDecoder: JSONDecoder) -> Self {
        self.jsonDecoder = jsonDecoder
        return self
    }
}

extension Network {
    class func Request(_ method: HTTPMethod,
                       _ url: String,
                       _ params: [String: Any]?,
                       _ headers: HTTPHeaders?,
                       _ decoder: JSONDecoder = JSONDecoder(),
                       success: @escaping SuccessHandlerType,
                       fail: @escaping FailureHandlerType) {
        
        let baseUrl = url
        var requestHeaders = HTTPHeaders()
        if let header = headers {
            requestHeaders = header
        } else {
            requestHeaders.add(name: "Content-Type", value: "application/json;charset=utf-8")
            requestHeaders.add(name: "Accept", value: "application/json")
        }
      
        AF.sessionConfiguration.timeoutIntervalForRequest = 20
        AF.request(baseUrl,
                   method: method,
                   parameters: params,
                   encoding: URLEncoding.default,
                   headers: requestHeaders,
                   interceptor: nil,
                   requestModifier: nil)
        .validate()
        .responseDecodable(decoder: decoder) { (response: AFDataResponse<New>) in //TODO: 泛型
            
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                print("Error: \(error)")
            }
            
        }
 
    }
}
