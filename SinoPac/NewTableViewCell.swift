//
//  newTableViewCell.swift
//  SinoPac
//
//  Created by Wen Chi on 2024/2/24.
//

import UIKit
import SnapKit
import Kingfisher

class NewTableViewCell: UITableViewCell {

    static let identifier: String = "NewTableViewCell_identifier"
    var isExpanded: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var dateLabel = UILabel()
    var titleLabel = UILabel()
    var newImageView = UIImageView()
    var contentLabel = UILabel()
    
    private func setupCell() {
        
        dateLabel.numberOfLines = 0
        dateLabel.textColor = .sinoPacBlue
        titleLabel.numberOfLines = 0
        titleLabel.textColor = .white
        contentLabel.numberOfLines = 0
        contentLabel.textColor = .sinoPacYellow

        addSubview(newImageView)
        addSubview(dateLabel)
        addSubview(titleLabel)
        addSubview(contentLabel)
        
//        newImageView.backgroundColor = .lightGray
//        dateLabel.backgroundColor = .lightGray
//        titleLabel.backgroundColor = .lightGray
//        contentLabel.backgroundColor = .lightGray

        newImageView.snp.makeConstraints { make in
            make.top.equalTo(10)
            make.right.equalTo(-10)
            make.width.height.equalTo(40)
        }
        
        dateLabel.snp.makeConstraints { make in
            make.top.equalTo(10)
            make.left.equalTo(10)
            make.right.equalTo(newImageView.snp.left).offset(-10)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(dateLabel.snp.bottom).offset(5)
            make.left.equalTo(10)
            make.right.equalTo(newImageView.snp.left).offset(-10)
        }
        
        contentLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.trailing.equalTo(newImageView.snp.leading).offset(-10)
            make.bottom.equalTo(-10)
        }
        
    }
    
    func configure(with article: Article){        
        dateLabel.text = article.publishedAt.string() //TODO: 改在整理資料前就處理
        titleLabel.text = article.title
        setImage(urlString: article.urlToImage)
    }
    
    func setImage( urlString: String ){
        guard let url = URL(string: urlString) else { return }
        newImageView.kf.setImage(with: url, options: [.diskCacheExpiration(.days(10))])
    }
    
}
