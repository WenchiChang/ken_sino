//
//  AppDelegate.swift
//  SinoPac
//
//  Created by Wen Chi on 2024/2/23.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window?.rootViewController = NewsViewController()
        window?.makeKeyAndVisible()
        return true
    }
    
    
}
