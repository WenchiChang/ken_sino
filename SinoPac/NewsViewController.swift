//
//  NewsViewController.swift
//  SinoPac
//
//  Created by Wen Chi on 2024/2/23.
//

import UIKit
import SnapKit
import Alamofire
import ProgressHUD

class NewsViewController: UIViewController {
    
    var newsTableView: UITableView = UITableView()

    var articles: [Article] = [] {
        didSet{
            newsTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupNewTableView()
        
        //抓取資料
        getNewDate()
        
    }
    
    func setupNewTableView() {
        newsTableView.register(NewTableViewCell.self, forCellReuseIdentifier: NewTableViewCell.identifier)
        newsTableView.delegate = self
        newsTableView.dataSource = self
        view.addSubview(newsTableView)
        newsTableView.snp.makeConstraints { (make) -> Void in
            make.edges.equalToSuperview()
        }
    }
    
    func loadJson(fileName: String) -> New? {
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        guard
            let url = Bundle.main.url(forResource: fileName, withExtension: "json"),
            let data = try? Data(contentsOf: url),
            let news = try? decoder.decode(New.self, from: data)
        else {
            return nil
        }
        
        return news
    }
    
    func getAPIKey() -> String? {
        
        guard let path = Bundle.main.path(forResource: "keys", ofType: "plist") else {
            return nil
        }
        
        guard let dict = NSDictionary(contentsOfFile: path), let apiKey = dict["apiKey"] as? String else {
            return nil
        }
        
        return apiKey
        
    }
    
    func getNewDate(){
        
        guard let apiKey = getAPIKey() else {
            print("!!! Get APIKey Fail !!!")
            return
        }
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        ProgressHUD.animate("資料讀取中...")
        
        Network.Request(.get, "https://newsapi.org/v2/top-headlines", ["sources":"techcrunch","apiKey":"\(apiKey)"], nil, decoder) { data in
            ProgressHUD.dismiss()
            print(data)
            self.articles = data.articles
        } fail: { code, message in
            ProgressHUD.dismiss()
            print(code)
            print(message)
        }
    }
    
}

extension NewsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NewTableViewCell.identifier, for: indexPath) as? NewTableViewCell else {
            fatalError("The tabelview could not dequeue a CustomCell in Viewcontroller.")
        }
        cell.configure(with: articles[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        guard let cell = tableView.cellForRow(at: indexPath) as? NewTableViewCell else {
            return
        }
        
        cell.contentLabel.text = cell.isExpanded ? "" : articles[indexPath.row].description
        cell.isExpanded.toggle()

        tableView.performBatchUpdates(nil)

    }
}
