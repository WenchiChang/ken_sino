//
//  DateExtension.swift
//  SinoPac
//
//  Created by Wen Chi on 2024/3/1.
//

import Foundation

extension Date {
    
    //2023-05-18 18:18
    func string() -> String{
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return df.string(from: self)
    }
    
}
